#include <stdio.h>

int main()
{
    int Arr[100], n, i, sum = 0;

    printf("Enter the number of elements you want to insert : ");
    scanf("%d", &n);

    for (i = 0; i < n; i++)
    {
        printf("Enter element %d : ", i + 1);
        scanf("%d", &Arr[i]);
        sum += Arr[i];
    }
    printf("\nThe Mean of the array elements is : %0.2f", (float)sum / n);
    printf("\nElements greater than mean are:");
    for (i = 0; i < n; i++)
    {
      if(Arr[i]>((float)sum/n))
      printf("\n%d",Arr[i]);
    }
    return 0;
}