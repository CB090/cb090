#include <stdio.h>

struct employee{
    char    name[30];
    int     empId;
    float   salary;
};
 
int main()
{
    int n;
    printf("Enter No. of Employee's:");
    scanf("%d",&n);
    struct employee emp[n];
    for(int i=0;i<n;i++)
    {
    printf("\nEnter details :\n");
    printf("Name ?:");        
    scanf("%s",&emp[i].name);
    printf("ID ?:");          
    scanf("%d",&emp[i].empId);
    printf("Salary ?:");       
    scanf("%f",&emp[i].salary);
    }
    for(int i;i<n;i++)
    {
    if(emp[i].salary<=25000)
    {
    printf("\nEmployee detail of salary less than 25000:\n");
    printf("Name: %s"   ,emp[i].name);
    printf("\nId: %d"     ,emp[i].empId);
    printf("\nSalary: %f\n",emp[i].salary);
    }
    }
    return 0;
}