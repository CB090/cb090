#include <stdio.h>
#include <math.h>

int main()
{
   float a,b,c,d,x1,x2;
   printf("Input the value of a,b & c : ");
   scanf("%f%f%f",&a,&b,&c);
   d=b*b-4*a*c;
   if(d==0)
   {
     printf("Both roots are real and equal.\n");
     x1=-b/(2.0*a);
     x2=x1;
     printf("R1= %f\n",x1);
     printf("R2= %f\n",x2);
   }
   else if(d>0)
	{
	   printf("Both roots are real and distinct\n");
	   x1=(-b+sqrt(d))/(2*a);
	   x2=(-b-sqrt(d))/(2*a);
	   printf("R1= %f\n",x1);
    printf("R2= %f\n",x2);
	}
	else
	    printf("Roots are imaginary;\n R1=%f+i%f  R2=%f-i%f \n",-b/(2*a),sqrt(-d)/(2*a),-b/(2*a),sqrt(-d)/(2*a));
return 0;
 }
